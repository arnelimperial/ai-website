import React, { lazy } from "react";
import { HelmetProvider } from "react-helmet-async";
import { Switch, Route } from "react-router-dom";
//Lazy loading
const Header = lazy(() => import("./components/header/header.component"));
const Home = lazy(() => import("./pages/homepage/homepage.component"));
const About = lazy(() => import("./pages/aboutpage/aboutpage.component"));
const Resume = lazy(() => import("./pages/resumepage/resumepage.component"));
const Uses = lazy(() => import("./pages/usespage/usespage.component"));
const Footer = lazy(() => import("./components/footer/footer.component"));
const SinglePost = lazy(() =>
  import("./pages/singlepost/singlepost.component")
);
const Contact = lazy(() => import("./pages/contactpage/contactpage.component"));
const NotFound = lazy(() => import("./pages/notfoundpage/notfound.component"));

function App() {
  return (
    <div className="holder">
      <HelmetProvider>
  
          <Header />
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/about" component={About}></Route>
            <Route path="/resume" component={Resume}></Route>
            <Route path="/uses" component={Uses}></Route>
            <Route path="/post/:id" component={SinglePost}></Route>
            <Route path="/contact" component={Contact}></Route>
            <Route component={NotFound}></Route>
          </Switch>
          <Footer name="Arnel" />
    
      </HelmetProvider>
    </div>
  );
}

export default App;
