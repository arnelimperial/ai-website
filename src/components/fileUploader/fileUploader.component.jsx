import React, { useRef } from "react";

const FileUploader = () => {
  const fileInput = useRef(null);

  const handleFileInput = (e) => {
    const file = e.target.files[0];
    if (file.size > 10240) onFileSelectError({ error: "File more than 10 MB" });
    else onFileSelectSuccess(file);
  };
};

export default FileUploader;