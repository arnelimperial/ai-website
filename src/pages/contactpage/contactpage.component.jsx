import React from "react";
import axios from "axios";
import "./contactpage.styles.scss";
import csrftoken from "../../common/csrf_token";

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

//const csrftoken = csrftoken;

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      subject: "",
      email: "",
      message: "",
      errors: {
        first_name: "",
        last_name: "",
        subject: "",
        email: "",
        message: "",
      },
    };
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case "first_name":
        errors.first_name =
          value.length < 2
            ? "First Name must be at least 2 characters long!"
            : "";
        break;
      case "last_name":
        errors.last_name =
          value.length < 2
            ? "Last Name must be at least 2 characters long!"
            : "";
        break;
      case "subject":
        errors.subject =
          value.length < 4 ? "Subject must be at least 4 characters long!" : "";
        break;
      case "email":
        errors.email = validEmailRegex.test(value) ? "" : "Email is not valid!";
        break;
      case "message":
        errors.message =
          value.length < 4 ? "Message must be at least 4 characters long!" : "";
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (validateForm(this.state.errors)) {
      console.log("Valid Form");
    } else {
      console.log("Invalid Form");
    }
    const data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      subject: this.state.subject,
      email: this.state.email,
      message: this.state.message,
    };

    axios
      .post(process.env.REACT_APP_ENDPOINT_MESSAGES, data)
      .then((response) => {
        let successmsg = document.querySelector(".success--msg");
        successmsg.innerHTML = `Thank you ${this.state.first_name}. Form has been submitted`;
      })
      .catch((error) => {
        let successmsg = document.querySelector(".success--msg");
        successmsg.innerHTML = `Thank you ${this.state.first_name}. Form has been submitted`;
      });

    this.setState({
      first_name: "",
      last_name: "",
      subject: "",
      email: "",
      message: "",
    });
  };

  render() {
    const { errors } = this.state;
    return (
      <main>
        <section>
          <h3 className="h3">Contact</h3>
          <form method="POST" onSubmit={this.handleSubmit}>
            {/* <CSRFToken /> */}
            <fieldset className="contact--fieldset">
              <div className="first--name">
                <label htmlFor="first-name-field">First Name</label>
                <input
                  type="text"
                  value={this.state.first_name}
                  name="first_name"
                  placeholder="First name"
                  className="contact--field"
                  onChange={this.handleChange}
                  noValidate
                />
                {errors.first_name.length > 0 && (
                  <span className="error">{errors.first_name}</span>
                )}
              </div>

              <div>
                <label htmlFor="last-name-field">Last Name</label>
                <input
                  type="text"
                  value={this.state.last_name}
                  name="last_name"
                  placeholder="Last name"
                  className="contact--field"
                  onChange={this.handleChange}
                  noValidate
                />
                {errors.last_name.length > 0 && (
                  <span className="error">{errors.last_name}</span>
                )}
              </div>
              <div>
                <label htmlFor="subject-field">Subject</label>
                <input
                  type="text"
                  value={this.state.subject}
                  name="subject"
                  placeholder="Your main concern."
                  className="contact--field"
                  onChange={this.handleChange}
                  noValidate
                />
                {errors.subject.length > 0 && (
                  <span className="error">{errors.subject}</span>
                )}
              </div>
              <div>
                <label htmlFor="email-field">Email</label>
                <input
                  type="email"
                  value={this.state.email}
                  name="email"
                  placeholder="A valid email"
                  className="contact--field"
                  onChange={this.handleChange}
                  noValidate
                />
                {errors.email.length > 0 && (
                  <span className="error">{errors.email}</span>
                )}
              </div>
              <div>
                <label htmlFor="message-field">Message</label>
                <textarea
                  value={this.state.message}
                  name="message"
                  onChange={this.handleChange}
                  className="contact--field contact--message"
                  placeholder="Message here..."
                  id=""
                  noValidate
                ></textarea>
                {errors.message.length > 0 && (
                  <span className="error">{errors.message}</span>
                )}
              </div>
              {/* <div>
                <label htmlFor="attachment-field">Attachment (If any)</label>
              <input type="file" ref={this.attachment} className="fileupload" />

              </div> */}
              <div className="contact--button">
                <button type="submit" className="btn btn-lg btn-block">
                  SUBMIT
                </button>
              </div>
              <div className="success--msg">
                <label htmlFor=""></label>
              </div>
            </fieldset>
          </form>
        </section>
      </main>
    );
  }
}

export default Contact;
