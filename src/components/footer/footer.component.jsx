import React from "react";

const Footer = () => (
  <footer>
    <small>
      2019 - {new Date().getFullYear()} by <b>Arnel Imperial</b>
    </small>
  </footer>
);

export default Footer;
